// http://www.redhat.com/magazine/003jan05/features/dbus/#low-level-c-listservice
#include <assert.h>          // for assert
#include <stdio.h>           // for stderr
#include <stdlib.h>          // for exit()
#include <dbus/dbus.h>       // for dbus_*

int
main (int argc, char **argv) {
    DBusConnection *connection;
    DBusError error;
    DBusMessage *message;
    DBusMessage *reply;
    int reply_timeout;
    char **service_list;
    int service_list_len;
    int i;

    dbus_error_init (&error);

    connection = dbus_bus_get (DBUS_BUS_SYSTEM,
            &error);
    //  FILE *stderr;
    //  stderr = fopen( "error.txt", "w" );
    //  if( stderr == NULL ) exit(1);

    if (connection == NULL) {
        fprintf(stderr, "Failed to open connection to bus: %s\n",
                error.message);
        dbus_error_free (&error);
        exit (1);
    }

    /* Construct the message */
    message = dbus_message_new_method_call ("org.freedesktop.DBus",       /*service*/
            "/org/freedesktop/DBus",     /*path*/
            "org.freedesktop.DBus",      /*interface*/
            "ListNames");

    /* Call ListServices method */
    reply_timeout = -1;   /*don't timeout*/
    reply = dbus_connection_send_with_reply_and_block (connection,
            message, reply_timeout,
            &error);

    if (dbus_error_is_set (&error)) {
        fprintf (stderr, "Error: %s\n",
                error.message);
        exit (1);
    }

    /* Extract the data from the reply */

    /* To append an array of fixed-length basic types (except Unix file descriptors),
     * pass in the DBUS_TYPE_ARRAY typecode, the element typecode,
     * the address of the array pointer, and a 32-bit integer giving the number of elements in the array
     * http://dbus.freedesktop.org/doc/api/html/group__DBusMessage.html
     */
    if (!dbus_message_get_args (reply, &error,
                DBUS_TYPE_ARRAY, DBUS_TYPE_STRING, &service_list,
                &service_list_len,
                DBUS_TYPE_INVALID)) {
        fprintf (stderr, "Failed to complete ListServices call: %s\n",
                error.message);
        exit (1);
    }
    dbus_message_unref (reply);
    dbus_message_unref (message);

    /* Print the results */

    printf ("Services on the message bus:\n");
    i = 0;
    while (i < service_list_len) {
        assert (service_list[i] != NULL);
        printf ("  %s\n", service_list[i]);
        ++i;
    }
    assert (service_list[i] == NULL);

    for (i = 0; i < service_list_len; i++)
        free (service_list[i]);
    free (service_list);

    return 0;
}
